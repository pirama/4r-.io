 (function () {

     var parallax = document.querySelectorAll(".parallax"),
         speed = 0.5;

     window.onscroll = function () {
    [].slice.call(parallax).forEach(function (el, i) {

             var windowYOffset = window.pageYOffset,
                 elBackgrounPos = "50% " + (windowYOffset * speed) + "px";
             if ($(window).width() > 700) {
                 el.style.backgroundPosition = elBackgrounPos;
             }

         });
     };

 })();